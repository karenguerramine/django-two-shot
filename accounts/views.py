from cmath import log
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib import messages


# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            # user = form.save(commit=False)  # we save the form because we want
            # to access the user object 
            username = request.POST.get("username")
            password = request.POST.get("password")
            user = User.objects.create_user(
                username=username, password=password
            )
            user.username = user.username.lower()
            user.save()
            login(request, user)  # when a user registers, they are
            # automatically logged in after
            return redirect("home")
        else:
            messages.error(
                request, 'An error occurred during the registration.'
                )
    else:
        form = UserCreationForm()
    
    return render(request, 'registration/signup.html', {'form': form})
